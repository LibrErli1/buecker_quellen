{{LineCenterSize|160|20|'''Feuilleton.'''}}


{{Linie}}


{{LineCenterSize|120|20|'''An den Quellen des Schwechater Biers'''}}
{{LineCenterSize|100|20|Von Friedrich Bücker.}}

{{BlockSatzStart}}

Die guten Wiener hatten mir die berühmten Kasematten (Lagerkeller) Anton Drehers in Kleinschwechat als so bombenfest und unzugänglich geschildert, das ich mich mit dem gröbsten Geschüz schwersten Kalibers versah, um sie zu forciren. Statt des modernen Gusstahls wählte ich diesmal aber das alte erprobte Gußilber und rasselte siegesgewiß mit meiner schweren Guldenbatterie, als ich den bunten Omnibus bestieg, der die bedeutungsvolle Aufschrift trug: „Schwechat-Wien.“

Meine Fahrgenossen waren, nach ihren Physiognomien zu urtheilen, sämmtlich Brauer, die alle in die Braugeheimnisse Kleinschwechats eindringen wollten, um dem berühmten Braukönig Konkurrenz zu machen.

Ich versuchte, mit dem einen und dem andern ein Gespräch anzuknüpfen, deutete auf das imposante k.k. Artilleriearsenal, daran wir links, und auf die Pulverthürme, daran wir rechts vorüberfuhren, aber sie blieben einsilbig und gestanden mit ihren nikenden Vollmondsgesichtern, daß sie in Wien und Umgegend unbekannt seien. Als ich aber über Malz, Braupfannen und Lagerkeller sprach, lösten sich ihre Zungen und sie bestürmten mich mit Fragen über Kleinschwechat. Sie wollten vor allen Dingen wissen, ob es eine Thatsache ist, dass der Vicekönig von Egypten bei seinem letzten Besuche in Wien sich vergebens bemüht, die Etablissements Drehers in Schwechat besichtigen zu dürfen, doch jezt hielt ich den Mund und ließ die Redseligen inmitten ihrer Neugier sizen.

Nach einstündiger Fahrt über den gepflasterten Schwechater „Rennweg“ tauchte das Dorf auf, dessen Bier die ganze Welt kennt und die halbe Welt trinkt.

Weder hochragende Maschinenschornsteine, noch bedeutende, von Thürmen flankirte, zinnengekrönte Baulichkeiten machten im voraus darauf aufmerksam, daß man sich großartigen Etablissements nähere. Ich wußte es, Drehers Reich durfte nicht in den Lüften, sondern mußte hauptsächlich in der Erde gesucht werden. Der Omnibus hielt, ich stieg aus und fragte den Kutscher rasch: „Welcher Weg führt zu Dreher?“ Er zeigte, statt zu antworten, mit der Peitsche geradeaus, und ich verfolgte rasch und allein die angedeutete Richtung, um mir von dem sieben Mann starken Brauerkorps, das mich begleitete, nicht den Vortritt nehmen oder gar das Terrain unsicher machen zu lassen. Nach etwa 60 schritten gähnten zur Rechten große Gewölbebogen, ich sah in zwei, drei riesige Höfe, in welche Bahngeleise hineinführten. Waggons wurden durch Arbeitertrupps in Bewegung gesezt, schneeweiße Stiere zogen faßbeladene Wagen aus einem Hof in den andern. Hämmertakt ertönte, als wenn hunderte von Küfern die Fässer für ein Schweizerschüzenfest bearbeiteten... Schwechat!

Da stand auch schon der Cerberus, der den Eingang in den Dreher'schen Tartarus bewachte. Ich überrumpelte ihn mit dem Befehl: „Melden Sie sofort einen Herrn
bei der Frau Witwe Dreher !“

„Die ist so früh am Morgen noch nicht zu sprechen“, antwortete er, dann deutete er stumm auf eine lange Tafel an der Wand der Gewölbemauer und nahm auf einer Bank Plaz Die Tafelinschrift befahl jedem stellesuchenden und abgewiesenen Brauer sofort das Brauhaus zu meiden, verhieß aber auch jedem wandernden Braugenossen einen unentgeltichen Schwechat'schen Labetrunk, der unter Vorzeigung von Schwechatmarken außerhalb der Dreher'schen Anlagen in einem nachbarlichen Garten eingenommen werden konnte.

Schon wollte ich mit meiner schweren Guldenbatterie in der Tasche rasseln, da ging etwa 20 Schritte von mir ein Herr über den Hof, der zur Schwechater Braukanzlei gehören mußte; jedenfalls deutete dies die Feder hinter dem Ohr an. Sofort ließ ich den wachehabenden Cerberus unbeachtet, forcirte das Gewölbe, näherte mich dem Herrn und bat ihn, mich zu dem Generaldirigenden der Brauerei zu führen. Er war sehr willfährig, und nach einigen Kreuz- und Querzügen stand ich mitten in der Kanzlei unter einer Anzahl Herren, denen mit Komplimenten nicht gedient war, sondern die vor allen Dingen wissen wollten, ob ich Brauer sei oder nicht.

{{PRZU}}

{{BlockSatzEnd}}

