Nach der Wanderung durch die Bottichräume ging es ein gut Stük tiefer in die Erde hinein, und zwar zu den Malztennen oder Gerstenkeimkammern.

Jezt begann das primitive schwankende Talglicht sein trügerisches Geflimmer, und nach der Wanderung durch drei, vier dieser mächtigen unterirdischen Gewölbe, darin sich der Uneingeweihte schwerer zurechtfindet, wie in der Dechenhöhle bei Iserlohn, ließ es den Kopf so stark hängen, daß es erlosch. Ich rief, denn ich war in dem Augenblik, als das Licht erlosch, meinem Führer weit voraus. Dumpfer Widerhall antwortete, aber ich verstand nichts. Unverständliche Fragen und noch unverständlichere Antworten in der pechschwarzen Finsterniß machten die Grotten, darin sich das einmal wachgerufene Echo nicht wieder beruhigen wollte, plözlich zu einem unheimlichen Aufenthalt. Ich ertastete mit einem Stok die nächste Gewölbemauer und ging dann die Mauer entlang nach der Richtung, in der mein Führer zurükgeblieben.

Jezt hatte der eine Tunnel ein Ende und ein zweiter gähnte mit seiner Mündung – so fühlte ich – entgegen. Der Führer mußte sich aber entfernt haben, denn ich erhielt auf meine Frage jezt nicht einmal mehr eine unverständliche Antwort. Wenn nur nicht etwa der Vicekönig von Egypten angekommen war und der Führer, der jedenfalls nach der Oberwelt hinaufgegangen, um Zündhölzchen und ein steiferes Licht zu holen, durch den festlichen Aufzug etwa vergaß, daß er einen Fremden in den Malztennen zurükgelassen – dann war schon alles gut. Da flammte es in der Ferne blizartig auf; ich sah in dem Hintergrunde des Tunnels, dessen Mündung ich mich eben genähert, eine Gasflamme brennen und meinen Führer darunter stehen.

Jezt hatte alle Besorgniß ein Ende, die Tunnels konnten sämmtlich durch Gas erleuchtet werden, und die Direktion hatte mir zu Ehren den Gashahn droben geöffnet. Bei der fernern Wanderung zeigte es sich, daß einige der Malztennen auch idyllisch mit der Oberwelt korrespondirten. Sonnenglanz und Laubgrün leuchtete hernieder, und ich wanderte in der That unter einem üppigen Garten dahin.

Nach der Malztennenwanderung sollte der bedenklichere Abstieg in die noch tieferen Lagerkeller, die sich bis Großschwechat vertheilen, unternommen werden.

Ich nenne den Abstieg bedenklich, denn droben herrschte eine Tropenglut von 30 Grad, und drinnen in den Kellern lag Reaumur fast auf dem Nullpunkt.

Bis zur ersten Gletscherwand, die die erste Partie des aus 28 Abtheilungen bestehenden ersten großen Lager-Kellers kühlte, wollte ich doch vordringen. Ich wikelte mich fest in meinen Plaid und folgte dem Talglicht, das ich auf eine Striknadel hatte steken lassen, in die Tiefe. Die Treppe, welche spiralförmig und klaftertief hinableitete, führte mich etwa von 4 zu 4 Stufen immer um einen Grad dem Gefrierpunkt näher.

Der Keller selbst kannte weder Schloß noch Riegel, sondern empfahl sich mit seiner Finsterniß, Kälte und Kohlensäure sofort jedem, der die Treppe überwunden. Der Talglichtschimmer sank unmittelbar nach dem Passiren der Kellermündung bis zum mattesten Irrlichtglanz herab, und kein Lichtkünstler hätte die Kerze bis zum Hintergrunde des Kohlensäure-Reviers brennend erhalten können. Gerade wie die Gletscher in den Alpen trübe Gletscherbäche zu Thal senden, so schikt auch die Gletscherwand in den Lager-Kellern Schwechats Rinnsale nieder, die sich dort, wo der Keller am tiefsten ist, zu morastartigen Teichen ansammeln, die am sichersten mit Stelzen passirt werden.

Am Tage ist's in den Kellern Schwechats still wie im Grabe, aber um 10 Uhr Nachts wird ein kleiner Theil der ausgedehnten Lagerräume aus seiner Ruhe aufgerüttelt. Von 10 Uhr bis um eine Stunde nach Mitternacht wird nämlich für das durstige Wien gezapft, das allein mehr als die Hälfte sämmtlichen Gerstensaftes konsumirt, den Dreher in Kleinschwechat fabrizirt. Um 4 Uhr Morgens kommt dann das Dampfroß von Wien, um den langen Bierzug mit den eis- und strohumhüllten Fässern der Kaiserstadt zuzuführen. Auf dem Raaber Bahnhofe Wiens halten schon die Gespanne, um den Nektar Drehers sofort nach Ankunft des Bierzuges durch Stadt und Vorstadt zu fahren, und um 7 Uhr Morgens hat ganz Wien den frischesten Trank aus Schwechats kühlen, unerschöpflichen Kellern.
