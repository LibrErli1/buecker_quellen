Als ich – umgekehrt wie in den Alpen – wieder von den Gletschern zu den Tropen emporgestiegen, besichtigte ich die Braupfannen und das Kühlschiff. Es war mir angenehm, daß in den Pfannen kein Biermeer wogte, sonst hätte ich nicht in ihre Tiefen hinabbliken können. Jezt war es mir, als sähe ich in einen Krater hinab, in dessen schlunde Vulkan mit seinen Gesellen arbeitete. Die schadhaften stellen der Pfannenböden wurden nämlich vernietet, und die Schmiede schlugen beim Schein einer qualmenden Oellampe tief unten im Pfannendunkel mit schweren Hämmern im Dreitakt auf das Metall, daß mir von dem Dröhnen fast die Sinne vergingen. Das schwechatsche kupferne Kühlschiff aber glich in seiner luftigen, ruhigen Höhe dem Dek eines Oceandampfers, doch hören hier sofort die Dekpassagierfreuden auf, sobald die zahlreichen Ventile gezogen werden, um das Kühlschiff zu füllen.

Am interessantesten war für mich die Wanderung die Malzlagerböden, und zwar nicht wegen der enormen Vorräthe, die hier aufgepeichert lagen, sondern weil ich hier noch die alten Uiberreste des kleinen Bräuhauses sah, welches den Vater des verflorbenen Anton Dreher angehörte. Mit sinniger Pietät ist das morsche Holzwerk in seiner ursprünglichen Anlage erhalten und dergestalt den Neubauten angereiht und einverleibt worden, daß es von leztern getragen und beschüzt wird. Das werdende Bier, welchees bei dem Großvater Dreher nur etliche Lachter weit zu laufen hatte, legt jezt einen Weg in Schwechat zurük, den ich fast auf eine halbe Meile schäze. Die Turbinen, welche von der Schwechat getrieben werden, sorgen aber im Verein mit den Dampfmaschinen dafür, daß das werdende Bier zum Passiren der unter- und überirdischen Rohrleitungen, die noch von Dampf- und Gasröhren begleitet werden, nicht viel mehr Zeit gebraucht, als zu Lebzeiten des Großvaters Dreher.

Leider sollte das statistische Wissen meines Führers mit der Angabe der Zahl der Gährbottiche seinen Höhepunkt und auch sein Ende erreicht haben. Zwar hatte er auf jede weitere Frage auch eine Antwort bei der Hand, aber ich merkte denn doch bald, daß er sich in einem höchst unsichern Fahrwasser bewegte und nur den Mund öffnete, um nicht für stumn zu gelten. Bald antwortete er vollends in den blauen Tag hinein, und erst, als ich ihm die verschiedensten Widersprüche nachwies, gestand er, daß er noch zu sehr Neuling in Schwechat sei, um alles wissen
zu können. Jezt hatte auch unsere Freundschaft ein Ende; ich zahlte ihm für die umsichtige Führung ein glänzendes Honorar, und er schied mit einer tiefen Verbeugung „Ew. Gnaden.“

Ich war eben im Begriff, das Direktorium zum Zweitenmate heimzusuchen, als ich die Frage hinter mir vernahm: „Nun, haben Sie sich gründlich umgesehen ?“ 

Ich wendete mich um und stand einem Herrn gegenüber, den ich in der Kanzlei bereits gegrüßt und der – wie ich jezt verrathen darf – in dem Hauptbuche Schwechats so bewandert ist, wie ich in meinem Notizbuch.

„Sie kommen mir mit Ihrer Frage sehr erwünscht“, antwortete ich, „nach meiner Uhr wandere ich bereits drei Stunden umher, weiß aber nicht, ob diese Zeit hinreicht, um alles gründlich zu sehen.“

„Nein, um die Drescher'schen Anlagen ganz zu durchwandern und nur flüchtig zu besichtigen, dürfte kaum ein halber Tag genügen. Die Lagerkeller allein nehmen so viel Zeit in Anspruch, als Sie zu Ihrer ganzen Rundtour verwendet. Und wenn erst die beiden Filiale Dreher's, das in Steinbruch bei Pest und das Filiale Micholup in Böhmen dazu kommen, so . . . .“

„Also in Klein- und Großschwechat hat das Dreher'sche Reich noch kein Ende?“ fiel ich den Herren in's Wort.

„Gewiß nicht, das Dreher'sche Filiale in Steinbruch bei Pest, welches Ungarn, den Orient und die Donau-Fürstenthümer mit Gerstensaft versorgt, wird Kleinschwechat bald überflügelt haben.“

{{PRZU}}
