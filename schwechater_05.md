„Sie sezen mich in Erstaunen; wie viel Bier produzirt denn das Pester Filiale jährlich?"

„Dreihundertsechzigtausend Eimer. Wir versenden keinen Tropfen von hier nach Ungarn und den Donau-Fürstenthümern.“

„Und wie viel beträgt die Jahresproduktion von Schwechat?"

„Fünfhundertachtzigtausend Eimer, und davon konsumirt Wien allein weit über die Hälfte.“

„O diese durstigen Seelen an der schönen blauen Donau! Und das Filiale in Böhmen?“

„Das ist das schwächste; es wird es bald auf hunderttausend Eimer gebracht haben, soll aber so vergrößert werden, daß von Böhmen aus ganz Norddeutschland mit Schwechater versorgt werden wird, während wir (Schwechat) das ganze übrige Oesterreich, Belgien, Holland, Frankreich, die Schweiz, Italien, England und Amerika
beherrschen.“

„In der That, und das alles wird im Winter produzirt! Ich glaubte hier mitten im Sommer die Brauerei im vollen Gange zu finden, wenigstens erinnert die in ganz Norddeutschland bekannte Bezeichnung: „Sommermärzen“ daran.“

Der Herr lächelte, dann fuhr er fort: „Das ominöse Wort : „Sommermärzen“ hat Schwechat nicht erdacht. Wir wissen hier weder etwas von Winter-, noch Wiener-, noch Sommerwärzen. Märzen heißt das berühmte Wort Anton Drehers, welches er schlicht und recht seinem Bier beilegte, weil er im März aufhörte zu brauen. Da sich aber sein zwölfgrädiges Bier nicht gut exportiren ließ, so braute er noch eine zwei Grad stärkere Gattung, nannte sie zwölfgrädige Lager und die vierzehngrädige Märzen, und dabei blieb's. Wir kennen zwar noch zwei feinere Nüancirungen: Abzug und Bok, aber diese haben für die Welt keine weitere Bedeutung.“

„Ich erinnere mich auch nicht, die Namen: Schwechater oder Märzen in Wien gehört zu haben."

„Wien kennt nur Lager, und alles exportirte Bier heißt Märzen.“

„Wenn ich nicht irre, hat sich Anton Dreher seine Höhe im Fluge erobert. Vor der Pariser Weltausstellung waren mir die Namen: Dreher und Schwechat noch böhmische Dörfer."

„Freilich, als Anton Dreher Ende 1863 starb, war sein Bier noch nicht weltberühmt. Der „Flug auf die Höhe“, wie Sie es zu nennen belieben, datirt seit der Pariser Weltausstellung. In Paris wurde zwar keine Seide gesponnen, aber die Bestellungen, die sofort nach Schluß der Pariser Ausstellung in Klein-Schwechat eingingen, nahmen in einer kurzen Spanne Zeit riesige Dimensionen an.“

„Braugeheimnisse kennt wohl Schwechat nicht?“

„Ganz und gar keine, nur die großen Mittel, mit denen wir agiren, und das treue Halten an dem Vorbilde Dreher's – darin bestehen unsere Braugeheimnisse. Der verstorbene Dreher war ein arger Feind der schweren dunkeln Biere, die den Kopf wüst und das Blut dik machen. Ein leichtes, helles, kräftigendes, erheiterndes Bier, ein Bier, das dem Fürsten ebenso mundet wie dem Handwerker, ein Nationalgetränk, ein Bier aller Welt, das war, was er zu erzielen suchte. Dreher reiste durch Baiern, Belgien, England, Ungarn u. s. w., kostete jedes Bier mit nüchterner Zunge, notirte sich die Wirkungen, prüfte, verwarf, ahmte nach und rastete nicht eher, bis er einen Trank brauen konnte, den er Jedermann empfehlen durfte. So entstand das Schwechater. Und wer, wie wir, auf denseiben Prinzipien fußt, alles groß und praktisch anlegt, Millionen daran verwendet, um uns Konkurrenz zu machen, der kann es.“

Ich war sehr erfreut über diese Mittheilungen, schied mit einem warmen Händedruk von meinem kompetenten Berichterstatter, und als ich noch von dem höchsten Punkte der Schwechater Etablissements einen Rundblik über die
13½ österreichische Joch umfassenden Dreher'schen Schöpfungen gethan, die Bahngeleise im Geiste verfolgt, die den echtesten Gerstentrank bis in alle Zonen verführen, ließ ich mich bei einem Glase kühlsten, aus dem Haupt-Keller geschöpften Schwechater nieder und schrieb das getheilte in mein Wanderbuch. „O Trank voll süßer Labe!“
